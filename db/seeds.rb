# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


authors = Author.create([
  {author_name: "aaaaa", phone_no: "11111111", email: "aaa@gmail.com", address: "AAAAAA"},
  {author_name: "bbbbb", phone_no: "22222222", email: "bbb@gmail.com", address: "BBBBBB"},
  {author_name: "ccccc", phone_no: "33333333", email: "ccc@gmail.com", address: "CCCCCC"},
  {author_name: "ddddd", phone_no: "44444444", email: "ddd@gmail.com", address: "DDDDDD"},
  {author_name: "eeeee", phone_no: "55555555", email: "eee@gmail.com", address: "EEEEEE"}
  ])


books = Book.create([
  {title: "title one", description: "description one", publication_date: "23-06-2006", price: 1000, rating: 1, in_stock: true, author_id: 1},
  {title: "title two", description: "description two", publication_date: "23-06-2006", price: 1000, rating: 1, in_stock: true, author_id: 1},
  {title: "title three", description: "description three", publication_date: "23-06-2005", price: 1000, rating: 1, in_stock: true, author_id: 2},
  {title: "title four", description: "description four", publication_date: "23-06-2004", price: 1000, rating: 1, in_stock: true, author_id: 2},
  {title: "title five", description: "description five", publication_date: "23-06-2005", price: 1000, rating: 1, in_stock: true, author_id: 3},
  {title: "title six", description: "description six", publication_date: "23-06-2003", price: 1000, rating: 1, in_stock: true, author_id: 3},
  {title: "title seven", description: "description seven", publication_date: "23-06-2001", price: 1000, rating: 1, in_stock: true, author_id: 4},
  {title: "title eight", description: "description eight", publication_date: "23-06-2005", price: 1000, rating: 1, in_stock: true, author_id: 4},
  {title: "title nine", description: "description nine", publication_date: "23-06-2004", price: 1000, rating: 1, in_stock: true, author_id: 5},
  {title: "title ten", description: "description ten", publication_date: "23-06-2016", price: 1000, rating: 1, in_stock: true, author_id: 5}
  ])
