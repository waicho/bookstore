class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :title
      t.text :description
      t.date :publication_date
      t.integer :price
      t.integer :rating
      t.boolean :in_stock
      t.integer :author_id

      t.timestamps
    end
  end
end
