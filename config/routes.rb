Rails.application.routes.draw do

  get "api/v1/author_list" => "api/version1#author"
  get "api/v1/book_list" => "api/version1#book"
  get "api/v1/book_show" => "api/version1#book_show"
  get "api/v1/author_show" => "api/version1#author_show"
  delete "api/v1/delete_book" => "api/version1#delete_book"
  delete "api/v1/delete_author" => "api/version1#delete_author"

  post "api/v1/post_author" => "api/version1#post_author"
  post "api/v1/post_book" => "api/version1#post_book"
  put "api/v1/update_book" => "api/version1#update_book"
  put "api/v1/update_author" => "api/version1#update_author"

  resources :books
  resources :authors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
