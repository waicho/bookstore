class BookSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :publication_date, :price, :rating, :in_stock, :author_id


  # def author_name
  #   object.attributes["author_id"].nil? ? "" : AuthorSerializer.new(object.author).attributes[:author_name]
  # end
end
