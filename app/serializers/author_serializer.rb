class AuthorSerializer < ActiveModel::Serializer
  attributes :id, :author_name, :phone_no, :email, :address
end
