class Api::Version1Controller < ApplicationController
  # http_basic_authenticate_with name: "admin", password: "123"

  skip_before_filter :verify_authenticity_token


  def author
    @authors = Author.all

    @author_list = Array.new

    @authors.all.each do |a|
          @author_list << AuthorSerializer.new(Author.find(a)).attributes
    end

    render json: @author_list, status: 200
  end


  def book
		@books = Book.all

    @book_list = Array.new

    @books.all.each do |b|
          @book_list << BookSerializer.new(Book.find(b)).attributes
    end


		render json: @book_list, status: 200
	end

  def book_show
    book_id = params[:id].to_i if params[:id] && !params[:id].blank?

    @book = BookSerializer.new(Book.find(book_id)).attributes
    render json: {"msg": "fail" }.to_json, status: 200 and return if @book.nil?

    render json: @book, status: 200

  end

  def author_show
    author_id = params[:id].to_i if params[:id] && !params[:id].blank?

    @author = AuthorSerializer.new(Author.find(author_id)).attributes
    render json: {"msg": "fail" }.to_json, status: 200 and return if @author.nil?

    render json: @author, status: 200

  end

  def post_author
    author_name = params[:author_name]
    phone_no = params[:phone_no]
    email = params[:email]
    address = params[:address]

    @author = Author.new(author_name: author_name, phone_no: phone_no, email: email, address: address)

    if @author.save
      msg = {"msg": "success"}
      render json: msg.to_json, status: 200
    else
      msg = {"msg": "Fail."}
      render json: msg.to_json, status: 401
    end

  end

  def post_book
    author_id = params[:author_id]
    title= params[:title]
    description = params[:description]
    publication_date = params[:publication_date]
    price = params[:price]
    rating = params[:rating]
    in_stock = params[:in_stock]
    # author_id = params[:author_id]

    @book = Book.new(title: title, description: description, publication_date: publication_date, price: price, rating: rating, in_stock: in_stock, author_id: author_id)

    if @book.save
      msg = {"msg": "success"}
      render json: msg.to_json, status: 200
    else
      msg = {"msg": "Fail."}
      render json: msg.to_json, status: 401
    end
  end

  def update_book
    id = params[:id].to_i

    @book = Book.find(id)

    title= params[:title].present? ? params[:title] : @book.title
    description = params[:description].present? ?  params[:description] : @book.description
    publication_date = params[:publication_date].present? ? params[:publication_date] : @book.publication_date
    price = params[:price].present? ? params[:price] : @book.price
    rating = params[:rating].present? ? params[:rating] : @book.rating
    in_stock = params[:in_stock].present? ? params[:in_stock] : @book.in_stock
    author_id = params[:author_id].present? ? params[:author_id] : @book.author_id

    if @book.present?

      @book.update_attributes(title: title, description: description, publication_date: publication_date, price: price, rating: rating, in_stock: in_stock, author_id: author_id)

      puts "*****************************"
      puts @book.to_json


      msg = {"msg": "success"}
      render json: msg.to_json, status: 200
    else
      msg = {"msg": "Fail."}
      render json: msg.to_json, status: 401
    end

  end

  def update_author
    id = params[:id].to_i

    @author = Author.find(id)

    author_name = params[:author_name].present? ? params[:author_name] : @author.author_name
    phone_no = params[:phone_no].present? ?  params[:phone_no] : @author.phone_no
    email = params[:email].present? ? params[:email] : @author.email
    address = params[:address].present? ? params[:address] : @author.address


    if @author.present?

      @author.update_attributes(author_name: author_name, phone_no: phone_no, email: email, address: address)

      puts "*****************************"
      puts @author.to_json


      msg = {"msg": "success"}
      render json: msg.to_json, status: 200
    else
      msg = {"msg": "Fail."}
      render json: msg.to_json, status: 401
    end

  end

  def delete_book
    book_id = params[:id].to_i if params[:id] && !params[:id].blank?

    # @book = BookSerializer.new(Book.find(book_id)).attributes
    @book = Book.find(book_id)
    @book.destroy
    # render json: {"msg": "fail" }.to_json, status: 200 and return if @book.nil?

    render json: {"msg": "success" }.to_json, status: 200
  end

  def delete_author
    author_id = params[:id].to_i if params[:id] && !params[:id].blank?

    # @book = BookSerializer.new(Book.find(book_id)).attributes
    @book = Book.all.where(author_id: author_id)
    @book.delete_all
    @author = Author.find(author_id)
    @author.destroy


    # render json: {"msg": "fail" }.to_json, status: 200 and return if @book.nil?

    render json: {"msg": "success. Author's books are deleted!" }.to_json, status: 200
  end

end
