json.extract! book, :id, :title, :description, :publication_date, :price, :rating, :in_stock, :author_id, :created_at, :updated_at
json.url book_url(book, format: :json)
