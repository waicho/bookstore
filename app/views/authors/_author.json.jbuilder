json.extract! author, :id, :author_name, :phone_no, :email, :address, :created_at, :updated_at
json.url author_url(author, format: :json)
